# Introduction 
TODO: Give a short introduction of your project. Let this section explain the objectives or the motivation behind this project. 

# Getting Started
TODO: Guide users through getting your code up and running on their own system. In this section you can talk about:
1.	Installation process
2.	Software dependencies
3.	Latest releases
4.	API references

# Build and Test
За настройку проекта отвечает файл application.properties.
Параметр server.port определяет порт для запуска сервиса. Используется встроеный сервлет контейнер, необходимый приложению.
Запуск сервиса производится выполненением batch файла start.bat если лень запускать ИДЕ(необходим Maven Plugin установленный на компьютере, Gradle Plugin не проверялся при запуске). 

В проекте подключен swagger, чтобы можно было вызывать api через интерфейс. Для пользования сваггера нужно зайти по адресу - http://localhost:8080/swagger-ui.html#/ (если порт 8080), выбрать из списка "api list" запрос, нажать на кнопку "try it out", ввести нужные параметры и кликнуть на кнопку execute для отправки запроса. В результате сваггер ниже выведет ответ от сервера (url, code, response body+response headers), переводенный текст располагается в response body.
# Contribute
Патчить базу через maven-plugins-liquibase:update. Автоматически база не патчится при старте, это настраивается в конфиге плагина в pom.xml
Патчить базу через maven-plugins-liquibase:update. Автоматически база не патчится при старте, это настраивается в конфиге плагина в pom.xml