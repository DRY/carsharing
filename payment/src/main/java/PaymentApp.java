import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Timofey Yakimov
 */
@SpringBootApplication(scanBasePackages = "rest")
public class PaymentApp {


    public static void main(String[] args) {
        SpringApplication.run(PaymentApp.class, args);
    }
}
