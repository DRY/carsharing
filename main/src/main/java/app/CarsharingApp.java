package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "app")
public class CarsharingApp {


    public static void main(String[] args) {
        SpringApplication.run(CarsharingApp.class, args);
    }

}