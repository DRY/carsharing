package app.web.service.Role;

import app.persistance.entity.Role;

import java.util.List;

public interface RoleService {

    List<Role> findAll();
}
