package app.web.service.Role;

import app.persistance.entity.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import app.web.repos.RoleRepo;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleServiceImpl(RoleRepo roleRepo) {
        this.roleRepo = roleRepo;
    }

    private final RoleRepo roleRepo;

    @Override
    public List<Role> findAll() {
        return roleRepo.findAll();
    }
}

