package app.web.service.BookListService;

import app.persistance.entity.Booking;
import app.web.pojo.Status;
import app.web.repos.BookingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class BookListServiceImpl implements BookListService {

    @Autowired
    BookListServiceImpl(BookingRepo bookingRepo) {
        this.bookingRepo = bookingRepo;
    }

    private final BookingRepo bookingRepo;

//    private static final String SELECT_FROM_BOOKING = "SELECT name, model, start_time, end_time, total_price, status"
//        + " FROM \"booking\", \"cars\", \"brands\" WHERE booking.car_id = cars.id"
//        + " AND cars.brand_id=brands.id AND user_id = ?";

//    private static final String SELECT_FROM_BOOKING = "SELECT booking.id, brands.name, models.name, date_from, date_to, booking.price, status"
//            + " FROM \"booking\", \"cars\", \"brands\", \"models\" WHERE booking.car_id = cars.id"
//            + " AND cars.brand_id=brands.id  AND cars.model_id=models.id AND user_id = ?";
//
//
//    private static final String SELECT_ALL_BOOKING = "SELECT booking.id, brands.name, models.name, date_from, date_to, booking.price, users.fullname, status"
//            + " FROM \"booking\", \"cars\", \"brands\", \"models\", \"users\" WHERE booking.car_id = cars.id AND booking.user_id = users.id"
//            + " AND cars.brand_id=brands.id  AND cars.model_id=models.id";
//
//    @Override
//    public List<Booking> getAll(Long userId) {
//        List<Booking> list = new ArrayList<>();
//        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/carsharing", "postgres", "postgres");
//             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_FROM_BOOKING)) {
//            preparedStatement.setLong(1, userId);
//            ResultSet resultSet = preparedStatement.executeQuery();
//            while (resultSet.next()) {
//                Booking booking = new Booking();
//                Car car = new Car();
//                Brand brand = new Brand();
//                booking.setId(resultSet.getLong(1));
//                brand.setName(resultSet.getString(2));
//                car.setModel(resultSet.getString(3));
//                car.setBrand(brand);
//                booking.setCar(car);
//                booking.setDateFrom(resultSet.getDate("date_from"));
//                booking.setDateTo(resultSet.getDate("date_to"));
//                booking.setPrice(resultSet.getDouble("price"));
//                booking.setStatus(Status.valueOf(resultSet.getString("status")));
//
//                list.add(booking);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return list;
//    }

    //TODO: Избавиться от jdbc. Перевести в Repo
//    public List<Booking> getAll() {
//        List<Booking> list = new ArrayList<>();
//        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/carsharing", "postgres", "postgres");
//             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_BOOKING)) {
//            ResultSet resultSet = preparedStatement.executeQuery();
//            while (resultSet.next()) {
//                Booking booking = new Booking();
//                Car car = new Car();
//                Brand brand = new Brand();
//                booking.setId(resultSet.getLong(1));
//                brand.setName(resultSet.getString(2));
//                car.setModel(resultSet.getString(3));
//                car.setBrand(brand);
//                booking.setCar(car);
//                booking.setDateFrom(resultSet.getDate("date_from"));
//                booking.setDateTo(resultSet.getDate("date_to"));
//                booking.setPrice(resultSet.getDouble("price"));
//                User user = new User();
//                user.setFullname(resultSet.getString("fullname"));
//                booking.setUser(user);
//                booking.setStatus(Status.valueOf(resultSet.getString("status")));
//
//                list.add(booking);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return list;
//    }

    @Override
    public List<Booking> findBookingByUserId(long userId) {
        return bookingRepo.findBookingByUserId(userId);
    }

    @Override
    public Booking findBookingById(long id) {
        return bookingRepo.findBookingById(id);
    }

    @Override
    public Double getCreditByUserId(long userId) {
        List<Booking> list = this.findBookingByUserId(userId);
        Double credit = 0.;
        for (Booking booking : list) {
            if (booking.getStatus() == Status.UNPAID) credit += booking.getPrice();
        }
        return credit;
    }

    @Override
    public List<Object[]> findAllByUserId(Long userId) {
        return bookingRepo.findAllByUserId(userId);
    }

    @Override
    public List<Object[]> findAllByDate(Date dateFrom, Date dateTo) {
        return bookingRepo.findAllByDate(dateFrom, dateTo);
    }

}
