package app.web.service.ModelService;

import app.persistance.entity.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import app.web.repos.ModelRepo;

import java.util.List;

@Service
public class ModelServiceImpl implements ModelService {

    @Autowired
    ModelServiceImpl(ModelRepo modelRepo) {
        this.modelRepo = modelRepo;
    }

    private final ModelRepo modelRepo;

    @Override
    public List<Model> findAll() {
        return modelRepo.findAll();
    }

    @Override
    public Model getById(Long id) {
        return modelRepo.findById(id).orElse(null);
    }
}
