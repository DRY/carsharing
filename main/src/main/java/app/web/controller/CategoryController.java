package app.web.controller;

import app.persistance.entity.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import app.web.service.CategoryService.CategoryService;

import java.util.Map;

@PreAuthorize("hasAuthority('Admin')")
@Controller
@RequestMapping("/admin/categories/")
public class CategoryController {

    @Autowired
    CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    private final CategoryService categoryService;

    @GetMapping("")
    public String list(Map<String, Object> model) {
        Iterable<Type> types = categoryService.getCategories();
        model.put("types", types);
        return "categories";
    }

    @GetMapping("add")
    public String add(Map<String, Object> model) {
        Type type = new Type();
        model.put("type", type);
        model.put("title", "Добавление категории");
        model.put("action", "/admin/categories/add");
        model.put("btn_action", "Добавить");
        return "add_edit_categories";
    }


    @PostMapping("add")
    public String add(@ModelAttribute("type") Type type) {
        categoryService.insertCategories(type);
        return "redirect:/admin/categories/";
    }

    @GetMapping("edit/{id}")
    public String edit(@PathVariable Long id,
                                 Map<String, Object> model) {
        Type type = categoryService.getCategoryById(id);
        model.put("type", type);
        model.put("title", "Редактирование категории");
        model.put("action", "/admin/categories/edit/" + type.getId());
        model.put("btn_action", "Сохранить");
        return "add_edit_categories";
    }

    @PostMapping("edit/{id}")
    public String edit(@ModelAttribute("type") Type type) {
        categoryService.updateCategories(type);
        return "redirect:/admin/categories/";
    }
}