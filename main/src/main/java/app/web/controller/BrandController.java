package app.web.controller;

import app.persistance.entity.Brand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import app.web.service.BrandService.BrandServiceImpl;

@PreAuthorize("hasAuthority('Admin')")
@Controller
@RequestMapping("/admin/brand/")
public class BrandController {

    @Autowired
    BrandController(BrandServiceImpl brandService){
        this.brandService = brandService;
    }

    private final BrandServiceImpl brandService;

    @GetMapping("")
    public ModelAndView list() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("list", brandService.getAll());
        modelAndView.setViewName("brand-list");
        return modelAndView;
    }

    @GetMapping("add")
    public ModelAndView add() {
        Brand brand = new Brand();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("brand", brand);
        modelAndView.setViewName("add-brand");
        return modelAndView;
    }

    @PostMapping("add")
    public String add(@ModelAttribute Brand brand) {
        brandService.save(brand);
        return "redirect:/admin/brand/";
    }

    @GetMapping("remove/{id}")
    public String remove(@PathVariable Long id) {
        brandService.delete(id);
        return "redirect:/admin/brand/";
    }

    @GetMapping("edit/{id}")
    public ModelAndView edit(@PathVariable Long id) {
        Brand brand = brandService.getById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("brand", brand);
        modelAndView.setViewName("edit-brand");
        return modelAndView;
    }


    @PostMapping("edit")
    public String edit(@ModelAttribute Brand brand) {
        brandService.edit(brand);
        return "redirect:/admin/brand/";
    }
}