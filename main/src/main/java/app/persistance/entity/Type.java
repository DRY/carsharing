package app.persistance.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "types")
public class Type {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;

    public Type() {
    }

    public Type(long id, String name) {
        this.id = id;
        this.name = name;
    }
}
