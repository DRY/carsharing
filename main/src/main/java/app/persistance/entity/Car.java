package app.persistance.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "brand_id", nullable = false)
    private Brand brand;

    private float price;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "model_id", nullable = false)
    private Model model;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "type_id", nullable = false)
    private Type type;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "city_id", nullable = false)
    private City city;

    private State state;
    private String regnumber;
    @OneToMany(targetEntity = Booking.class,
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "car_id")
    private List<Booking> bookings;

    @Column(name = "image")
    private String image;

    public Car() {
    }
}



