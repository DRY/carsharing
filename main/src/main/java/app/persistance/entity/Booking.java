package app.persistance.entity;

import app.web.pojo.Status;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "booking")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "user_id")
    private long userId;
    @Column(name = "car_id")
    private long carId;
    private Date dateFrom;
    private Date dateTo;
    private Double price;
    @Enumerated(EnumType.STRING)
    private Status status;

    public Booking() {
    }

    public Booking(long userId, long carId, Date dateFrom, Date dateTo
            , Double price
            , Status status
    ) {
        this.userId = userId;
        this.carId = carId;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.price = price;
        this.status = status;
    }
}
