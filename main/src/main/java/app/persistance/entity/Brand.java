package app.persistance.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "brands")
public class Brand {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;

    public Brand() {
    }

    public Brand(String name) {
        this.name = name;
    }

    public Brand(long id, String name) {
        this.id = id;
        this.name = name;
    }
}
