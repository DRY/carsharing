package app.persistance.entity;

import lombok.Data;

@Data
public class PayCard {

    private String name;
    private String number;
    private String expiration;
    private String code;

    public PayCard() {
    }
}
